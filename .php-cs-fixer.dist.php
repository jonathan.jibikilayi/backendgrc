<?php

$finder = (new PhpCsFixer\Finder())
    ->exclude('somedir')
    ->notPath('config/preload.php')
    ->notPath('public/index.php')
    ->notPath('src/tests/bootstrap.php')
    ->in(__DIR__)
;

return (new PhpCsFixer\Config())
    ->setRules([
        '@PSR12' => true,
        '@Symfony' => true,
        'full_opening_tag' => false,
    ])
    ->setFinder($finder)
;
<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220804135616 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user ADD country VARCHAR(50) NOT NULL, ADD function VARCHAR(50) NOT NULL, ADD status TINYINT(1) NOT NULL, ADD gender VARCHAR(25) DEFAULT NULL, ADD registration_number VARCHAR(25) NOT NULL, ADD salary INT NOT NULL, ADD category VARCHAR(25) DEFAULT NULL, ADD licence VARCHAR(25) NOT NULL, ADD hard DATE DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user DROP country, DROP function, DROP status, DROP gender, DROP registration_number, DROP salary, DROP category, DROP licence, DROP hard');
    }
}

<?php

declare(strict_types=1);

namespace App\Authorizations;

use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Security;

class UserAuthorizationChecker
{
    private array $methodeAllowed = [
        Request::METHOD_PUT,
        Request::METHOD_PATCH,
        Request::METHOD_DELETE,
    ];

    private ?User $user;

    public function __construct(Security $security)
    {
        $this->user = $security->getUser();
    }

    public function check(User $user, string $methode): void
    {
        $this->isAuthentificated();
        if ($this->isMethodeAllowed($methode) && $user->getId() !== $this->user->getId()) {
            $errorMessage = "It's not your ressource";
            throw new UnauthorizedHttpException($errorMessage, $errorMessage);
        }
    }

    public function isAuthentificated(): void
    {
        if (null === $this->user) {
            $errorMessage = 'YOU ARE NOT AUTHENTICATED';
            throw new UnauthorizedHttpException($errorMessage, $errorMessage);
        }
    }

    public function isMethodeAllowed(string $methode): bool
    {
        return in_array($methode, $this->methodeAllowed, strict: true);
    }
}

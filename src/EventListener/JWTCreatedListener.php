<?php

// declare(strict_types=1);

// namespace App\EventListener;

// use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTDecodedEvent;
// use Symfony\Component\Security\Core\Security;
// use Symfony\Component\Security\Core\User\UserInterface;

// class JWTCreatedListener
// {
//     // src/App/EventListener/JWTDecodedListener.php
//     /**
//      * @param JWTDecodedEvent $event
//      *
//      * @return void
//      */
//     public function onJWTDecoded(JWTDecodedEvent $event)
//     {
//         $payload = $event->getPayload();
//         $user = $this->userRepository->findOneByUsername($payload['username']);

//         $payload['custom_user_data'] = $user->getCustomUserInformations();

//         $event->setPayload($payload); // Don't forget to regive the payload for next event / step
//     }
// }

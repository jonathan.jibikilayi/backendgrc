<?php

namespace App\Events;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Authorizations\UserAuthorizationChecker;
use App\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class UserSuscriber implements EventSubscriberInterface
{
    private array $methodeNotAllowed = [
        Request::METHOD_POST,
        Request::METHOD_GET,
    ];

    private UserAuthorizationChecker $userAuthorizationChecker;

    public function __construct(UserAuthorizationChecker $userAuthorizationChecker)
    {
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['check', EventPriorities::PRE_VALIDATE],
        ];
    }

    public function check(ViewEvent $event): void
    {
        $user = $event->getControllerResult();
        $methode = $event->getRequest()->getMethod();

        if ($user instanceof User && !in_array($methode, $this->methodeNotAllowed, true)) {
            $this->userAuthorizationChecker->check($user, $methode);
            $user->setUpdatedAt(new \DateTimeImmutable());
        }
    }
}

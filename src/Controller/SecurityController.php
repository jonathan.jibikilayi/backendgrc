<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Attribute\CurrentUser;

class SecurityController extends AbstractController
{
    #[Route(path: '/api/login', name: 'api_login', methods: ['POST'])]
    public function login()
    {
        $user = $this->getUser();

        return $this->json([
            'username' => $user->getUserIdentifier(),
            'role' => $user->getRoles(),
        ]);
    }
    // private Security $security;

    // #[Route(path: '/api/login', name: 'api_login', methods: ['POST'])]
    // public function index(#[CurrentUser] ?User $user): Response
    // {
    //     if (null === $user) {
    //          return $this->json([
    //              'message' => 'missing credentials',
    //          ], Response::HTTP_UNAUTHORIZED);
    //      }

    //      $token = $this->security->getToken(); 
    //     // somehow create an API token for $user

    //     return $this->json([
    //         'id' => $user->getId(),
    //         'user'  => $user->getUserIdentifier(),
    //         'token' => $token,
    //     ]);
    // }

//     public function login(Request $request, UserRepository $userRepository, UserPasswordEncoderInterface $encoder)
//     {
//         $user = $userRepository->findOneBy([
//                 'email'=>$request->get('email'),
//         ]);
//         if (!$user || !$encoder->isPasswordValid($user, $request->get('password'))) {
//                 return $this->json([
//                     'message' => 'email or password is wrong.',
//                 ]);
//         }
//        $payload = [
//            "user" => $user->getUsername(),
//            "exp"  => (new \DateTime())->modify("+5 minutes")->getTimestamp(),
//        ];


//         $jwt = JWT::encode($payload, $this->getParameter('jwt_secret'), 'HS256');
//         return $this->json([
//             'message' => 'success!',
//             'token' => sprintf('Bearer %s', $jwt),
//         ]);
// }
}

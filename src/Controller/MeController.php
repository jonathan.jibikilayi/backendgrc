<?php

namespace App\Controller;

use App\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTAuthenticatedEvent;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Attribute\CurrentUser;

class MeController extends AbstractController
{
    // private Security $security;

    public function __invoke(#[CurrentUser] ?User $user): Response
    {
        if (null === $user) {
             return $this->json([
                 'message' => 'missing credentials',
             ], Response::HTTP_UNAUTHORIZED);
         }

        // $token = $this->security->getToken(); 
        // $token = $event->getToken(); 

        return $this->json([
            'id' => $user->getId(),
            'email'  => $user->getUserIdentifier(),
            'role' => $user->getRoles(),
            // 'token' => $token,
        ]);
    }
}

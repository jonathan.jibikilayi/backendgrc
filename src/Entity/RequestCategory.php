<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\RequestCategoryRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RequestCategoryRepository::class)]
#[ApiResource()]
class RequestCategory
{
    use RessourceId;

    #[ORM\Column(type: 'string', length: 50)]
    private $category_request_lib;

    #[ORM\Column(type: 'string', length: 5)]
    private $category_request_code;

    public function getCategoryRequestLib(): ?string
    {
        return $this->category_request_lib;
    }

    public function setCategoryRequestLib(string $category_request_lib): self
    {
        $this->category_request_lib = $category_request_lib;

        return $this;
    }

    public function getCategoryRequestCode(): ?string
    {
        return $this->category_request_code;
    }

    public function setCategoryRequestCode(string $category_request_code): self
    {
        $this->category_request_code = $category_request_code;

        return $this;
    }
}

<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\MeController;
use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ApiResource(
    collectionOperations: [
        'get' => [
            'normalization_context' => ['groups' => ['user:read']],
        ],
        'post'
    ],
    itemOperations: [
        'me' => [
            'pagination_enabled' => false,
            'path' => '/me',
            'method' => 'get',
            'controller' => MeController::class,
            'read' => false,
            'openapi_context' => [
                'security' => [['bearerAuth' => []]],
            ],
        ],
        'get' => [
            'normalization_context' => ['groups' => ['user:details:read']],
        ],
        'put',
        'patch',
        'delete'
    ]
),
ApiFilter(SearchFilter::class, properties: ['email' => 'partial'])
]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    use RessourceId;
    use Timestapable;

    #[ORM\Column(type: 'string', length: 180, unique: true)]
    #[Groups(['user:read', 'user:details:read'])]
    private $email;

    #[ORM\Column(type: 'json')]
    #[Groups(['user:read', 'user:details:read'])]
    private $roles = [];

    #[ORM\Column(type: 'string')]
    private $password;

    #[Groups(['user:read', 'user:details:read'])]
    #[ORM\Column(type: 'string', length: 255)]
    private $phone;

    #[Groups(['user:read', 'user:details:read'])]
    #[ORM\Column(type: 'string', length: 255)]
    private $firstname;

    #[Groups(['user:read', 'user:details:read'])]
    #[ORM\Column(type: 'string', length: 255)]
    private $lastname;

    #[ORM\Column(type: 'string', length: 50)]
    #[Groups(['user:read', 'user:details:read'])]
    private $country;

    #[ORM\Column(type: 'string', length: 50)]
    #[Groups(['user:read', 'user:details:read'])]
    private $function;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['user:read', 'user:details:read'])]
    private $status;

    #[ORM\Column(type: 'string', length: 25, nullable: true)]
    #[Groups(['user:read', 'user:details:read'])]
    private $gender;

    #[Groups(['user:read', 'user:details:read'])]
    #[ORM\Column(type: 'string', length: 25)]
    private $registration_number;

    #[ORM\Column(type: 'integer')]
    #[Groups(['user:read', 'user:details:read'])]
    private $salary;

    #[ORM\Column(type: 'string', length: 25, nullable: true)]
    #[Groups(['user:read', 'user:details:read'])]
    private $category;

    #[ORM\Column(type: 'string', length: 25)]
    #[Groups(['user:read', 'user:details:read'])]
    private $licence;

    #[ORM\Column(type: 'date', nullable: true)]
    #[Groups(['user:read', 'user:details:read'])]
    private $hard;

    #[ORM\Column(type: 'string', length: 25, nullable: true)]
    #[Groups(['user:read', 'user:details:read'])]
    private $city;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getFunction(): ?string
    {
        return $this->function;
    }

    public function setFunction(string $function): self
    {
        $this->function = $function;

        return $this;
    }

    public function isStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(?string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getRegistrationNumber(): ?string
    {
        return $this->registration_number;
    }

    public function setRegistrationNumber(string $registration_number): self
    {
        $this->registration_number = $registration_number;

        return $this;
    }

    public function getSalary(): ?int
    {
        return $this->salary;
    }

    public function setSalary(int $salary): self
    {
        $this->salary = $salary;

        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(?string $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getLicence(): ?string
    {
        return $this->licence;
    }

    public function setLicence(string $licence): self
    {
        $this->licence = $licence;

        return $this;
    }

    public function getHard(): ?\DateTimeInterface
    {
        return $this->hard;
    }

    public function setHard(?\DateTimeInterface $hard): self
    {
        $this->hard = $hard;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }
}
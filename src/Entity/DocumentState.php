<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\DocumentStateRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DocumentStateRepository::class)]
#[ApiResource()]
class DocumentState
{
    use RessourceId;
    use Timestapable;
}

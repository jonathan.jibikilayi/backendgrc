<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\RequestRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RequestRepository::class)]
#[ApiResource()]
class Request
{
    use RessourceId;
    use Timestapable;

    #[ORM\Column(type: 'string', length: 10)]
    private $request_incident_level;

    #[ORM\Column(type: 'text', nullable: true)]
    private $request_text;

    #[ORM\Column(type: 'string', length: 10)]
    private $request_scope;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $copy;

    #[ORM\Column(type: 'date', nullable: true)]
    private $implementation_date;

    #[ORM\Column(type: 'date')]
    private $start_request_date;

    #[ORM\Column(type: 'date')]
    private $end_request_date;

    public function getRequestIncidentLevel(): ?string
    {
        return $this->request_incident_level;
    }

    public function setRequestIncidentLevel(string $request_incident_level): self
    {
        $this->request_incident_level = $request_incident_level;

        return $this;
    }

    public function getRequestText(): ?string
    {
        return $this->request_text;
    }

    public function setRequestText(?string $request_text): self
    {
        $this->request_text = $request_text;

        return $this;
    }

    public function getRequestScope(): ?string
    {
        return $this->request_scope;
    }

    public function setRequestScope(string $request_scope): self
    {
        $this->request_scope = $request_scope;

        return $this;
    }

    public function getCopy(): ?int
    {
        return $this->copy;
    }

    public function setCopy(?int $copy): self
    {
        $this->copy = $copy;

        return $this;
    }

    public function getImplementationDate(): ?\DateTimeInterface
    {
        return $this->implementation_date;
    }

    public function setImplementationDate(?\DateTimeInterface $implementation_date): self
    {
        $this->implementation_date = $implementation_date;

        return $this;
    }

    public function getStartRequestDate(): ?\DateTimeInterface
    {
        return $this->start_request_date;
    }

    public function setStartRequestDate(\DateTimeInterface $start_request_date): self
    {
        $this->start_request_date = $start_request_date;

        return $this;
    }

    public function getEndRequestDate(): ?\DateTimeInterface
    {
        return $this->end_request_date;
    }

    public function setEndRequestDate(\DateTimeInterface $end_request_date): self
    {
        $this->end_request_date = $end_request_date;

        return $this;
    }
}

<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\DocumentResourceRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DocumentResourceRepository::class)]
#[ApiResource()]
class DocumentResource
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $document_read;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private $read_date;

    #[ORM\Column(type: 'datetime')]
    private $created_at;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDocumentRead(): ?int
    {
        return $this->document_read;
    }

    public function setDocumentRead(?int $document_read): self
    {
        $this->document_read = $document_read;

        return $this;
    }

    public function getReadDate(): ?\DateTimeInterface
    {
        return $this->read_date;
    }

    public function setReadDate(?\DateTimeInterface $read_date): self
    {
        $this->read_date = $read_date;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }
}

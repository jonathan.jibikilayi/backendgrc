<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\StatusRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: StatusRepository::class)]
#[ApiResource()]
class Status
{
    use RessourceId;

    #[ORM\Column(type: 'string', length: 10, nullable: true)]
    private $status_flag;

    #[ORM\Column(type: 'string', length: 50)]
    private $status_color;

    #[ORM\Column(type: 'integer')]
    private $status_order;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $status_next;

    #[ORM\Column(type: 'string', length: 50, nullable: true)]
    private $status_desc;

    public function getStatusFlag(): ?string
    {
        return $this->status_flag;
    }

    public function setStatusFlag(?string $status_flag): self
    {
        $this->status_flag = $status_flag;

        return $this;
    }

    public function getStatusColor(): ?string
    {
        return $this->status_color;
    }

    public function setStatusColor(string $status_color): self
    {
        $this->status_color = $status_color;

        return $this;
    }

    public function getStatusOrder(): ?int
    {
        return $this->status_order;
    }

    public function setStatusOrder(int $status_order): self
    {
        $this->status_order = $status_order;

        return $this;
    }

    public function getStatusNext(): ?string
    {
        return $this->status_next;
    }

    public function setStatusNext(?string $status_next): self
    {
        $this->status_next = $status_next;

        return $this;
    }

    public function getStatusDesc(): ?string
    {
        return $this->status_desc;
    }

    public function setStatusDesc(?string $status_desc): self
    {
        $this->status_desc = $status_desc;

        return $this;
    }
}

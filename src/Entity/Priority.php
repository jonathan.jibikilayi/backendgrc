<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\PriorityRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PriorityRepository::class)]
#[ApiResource()]
class Priority
{
    use RessourceId;

    #[ORM\Column(type: 'integer')]
    private $priority_value;

    public function getPriorityValue(): ?int
    {
        return $this->priority_value;
    }

    public function setPriorityValue(int $priority_value): self
    {
        $this->priority_value = $priority_value;

        return $this;
    }
}

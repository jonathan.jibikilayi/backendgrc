<?php

namespace App\Entity;

use App\Repository\ReproductivityRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ReproductivityRepository::class)]
class Reproductivity
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 50)]
    private $reproductivity_lib;

    #[ORM\Column(type: 'string', length: 5)]
    private $reproductivity_code;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReproductivityLib(): ?string
    {
        return $this->reproductivity_lib;
    }

    public function setReproductivityLib(string $reproductivity_lib): self
    {
        $this->reproductivity_lib = $reproductivity_lib;

        return $this;
    }

    public function getReproductivityCode(): ?string
    {
        return $this->reproductivity_code;
    }

    public function setReproductivityCode(string $reproductivity_code): self
    {
        $this->reproductivity_code = $reproductivity_code;

        return $this;
    }
}

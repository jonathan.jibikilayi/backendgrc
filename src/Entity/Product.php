<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ProductRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProductRepository::class)]
#[ApiResource()]
class Product
{
    use RessourceId;

    #[ORM\Column(type: 'integer')]
    private $product_specific;

    public function getProductSpecific(): ?int
    {
        return $this->product_specific;
    }

    public function setProductSpecific(int $product_specific): self
    {
        $this->product_specific = $product_specific;

        return $this;
    }
}

<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\DocumentMarkRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DocumentMarkRepository::class)]
#[ApiResource()]
class DocumentMark
{
    use RessourceId;
    use Timestapable;
}
